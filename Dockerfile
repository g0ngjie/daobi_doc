FROM nginx

WORKDIR /app

RUN cd /app && mkdir ucenter introduce light dissertation dynamic && \
    rm /etc/nginx/conf.d/default.conf && \
    rm /etc/nginx/nginx.conf && \
    rm /usr/share/nginx/html/index.html

COPY ucenter/_book /app/ucenter
COPY introduce/_book /app/introduce
COPY light/_book /app/light
COPY dissertation/_book /app/dissertation
COPY dynamic-router/_book /app/dynamic

COPY nginx.conf /etc/nginx/

COPY index.html /usr/share/nginx/html
COPY favicon.ico /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]



