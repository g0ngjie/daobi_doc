# classbro-u-center-web-v2.0

> ###### u-center-web 项目迁移 重构
>
> ###### node版本 10.16.0

###### 

#### 概要

> webpack版本2 迁移为版本4, 统一配置文件 [vue.config.js](https://www.webpackjs.com/configuration/)
>
> 启动脚本，node进程分发 `本地` `预发` `生产` 环境变量
>
> axios + webpack proxy 解决前端跨域，可多路由、多代理配置请求。
>
> 添加`eslint` 代码检测 、`Xss`防跨站脚本攻击
>
> Layout重构
>
> 国际化支持
>
> 菜单渲染、权限校验支持
>
> 拦截器 + 白名单功能
>
> 统一`工具集`、`枚举`、`vuex`、`国际化`配置规范



#### Vscode插件推荐

> **Vetur** 因为eslint的支持，对于代码格式规范，建议统一用 Vetur工具格式化代码
>
> **TODO Highlight**、**Todo Tree** 开发过程中可能会遇到暂时无法解决，待定的情况。



#### 项目目录结构

```bash
.
├── babel.config.js
├── config
│   └── index.js		# 环境变量
├── deploy.sh
├── docker-compose.yml
├── Dockerfile
├── jsconfig.json		# vscode编辑器下js的显示引用文件
├── LICENSE
├── nginx
│   └── default.conf
├── package.json		# 说明文件
├── public
│   ├── favicon.ico
│   └── index.html
├── README.md
├── src
│   ├── api				# 接口文件
│   ├── App.vue			# 根组件
│   ├── assets			# 资源文件
│   ├── components		# 公共组件库
│   ├── directives		# 全局指令
│   ├── filters			# 全局过滤器
│   ├── icons			# svg目录
│   ├── lang			# 国际化
│   ├── layout
│   ├── main.js			# 入口文件
│   ├── mixins			# 全局多继承
│   ├── router			# 路由
│   ├── store			# vuex
│   ├── styles			# 公共样式
│   ├── utils			# 工具集
│   └── views			# 视图文件夹
├── vue.config.js		# webpack配置文件
└── yarn.lock
```



#### 缓存处理

> 当新建页面或者添加文件的时候。有时会遇到一些 *undefined* 或者文件不存在的问题。
>
> 这类情况下可通过下面三种方式。

* webpack通过`cache-loader` 会默认为 Vue/Babel/TypeScript 编译开启。文件会缓存在 `node_modules/.cache` 中。通过命令 `rm -rf node_modules/.cache` 或者手动在`node_modules`下找到`.cache`文件，删除掉。
* 通过快捷键`ctrl` + `shift` + `delete` 清理浏览器缓存。
* 通过快捷键 `ctrl` + `f5` 强制刷新浏览器。



#### 规范

> 类似`styles`、`components`、`filter`、`directive`、`mixins` 等全局功能，尽量封装使用频率比较高的。仅仅使用一两此的不要抽象出来，建议当前目录里使用。
>
> 自定义全局样式统一封装到 `src/styles/custom.scss` 里，避免影响其他模块。
