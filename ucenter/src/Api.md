# 接口文件

> 目录地址：src/api
>
> 结合 src/utils/Request.js



#### 建议

> 目前项目大部分接口都写在 **daoBiApi.js**
>
> 为了避免所有功能写在一个文件里，**建议** `api`文件命名按照功能分模块

```bash
.
├── login.js
├── role.js
└── user.js
```



#### 请求示例

> 接口命名规范`尽量`按照**服务端**接口文档定义写，方便排查问题接口
>
> 例如：`/account/v1/resetPassword` => `account_resetPassword` || `accountResetPassword`

```javascript
import { Prefix } from "config";

/** 模拟Get */
export const auth_userInfo = (params) => {
    return request({
        url: `${Prefix}/auth/v1/userInfo`,
        method: "GET",
        params
    })
}

/** 模拟Post */
export const auth_addUser = (data) => {
    return request({
        url: `${Prefix}/auth/v1/addUser`,
        method: "POST",
        data
    })
}

/** 模拟Put */
export const auth_userUpdate = (data) => {
    return request({
        url: `${Prefix}/auth/v1/userUpdate`,
        method: "PUT",
        data
    })
}

/** 模拟Delete */
export const auth_delUser = (userId) => {
    return request({
        url: `${Prefix}/auth/v1/${userId}`,
        method: "DELETE"
    })
}
```

