# 权限校验

> 1、通过自定义全局的指令已经函数，控制用户的操作权限
>
> 2、菜单渲染需要结合拦截器使用



#### 权限校验指令 

> `v-permission`  目录地址 `src/directives/permission.js`
>
> 里面包含了指令 & 校验函数

```javascript
import store from "@/store";

function _permission(permissionRoles) {
  const roles = store.getters && store.getters.roles;
  const hasPermission = roles.some((role) => {
    return permissionRoles.includes(role);
  });
  return hasPermission;
}

export default {
  inserted(el, binding) {
    const { value: roles } = binding;
    if (roles && roles instanceof Array && roles.length > 0) {
      const hasPermission = _permission(roles);
      if (!hasPermission) el.parentNode && el.parentNode.removeChild(el);
    } else {
      throw new Error(`need roles! Like v-permission="['admin','editor']"`);
    }
  },
};

/**
 * @param {Array} value
 * @returns {Boolean}
 */
export const checkPermission = (roles) => {
  if (roles && roles instanceof Array && roles.length > 0) {
    const hasPermission = _permission(roles);
    if (!hasPermission) return false;
    return true;
  } else {
    console.error(`need roles! Like checkPermission="['admin','editor']"`);
    return false;
  }
};
```




> `main.js` 注册指令

```javascript
import permission from "./directives/index";

Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key]);
});
```



#### 指令使用

> `v-permission="['admin']"`

```javascript
<!-- 新增角色 -->
<el-button v-permission="['admin']">{{$t('roleManage.index.newRole')}}</el-button>
```



#### v-if 条件方式校验权限

> 由于 该指令实现原理是根据权限判断是否删除Dom元素。但是一些特殊情况下不起作用。
>
> 例如：`el-table-column` 
>
> 由于渲染的生命周期问题。el-table 是异步更新数据。这种情况下需要用到 `v-if`
>
> `v-if="checkPermission(['admin'])"`



```javascript
...

<!-- 岗位级别 -->
<el-table-column v-if="checkPermission(['admin'])" :label="$t('stationManage.stationTable.table.level')" />

    ...

<script>
import { checkPermission } from "@/directives/permission";
export default {
    methods: {
        checkPermission,
    }
}
...
```





#### 菜单动态渲染

> 思路：
>
> 拦截器：`src/utils/Interceptor.js` 通过路由守卫，`router.beforeEach` 
>
> 结合`Vuex/...permission文件` 和 `utils/Menus.js` 生成权限菜单

```javascript
router.beforeEach(async (to, _, next) => {
  NProgress.start();
  if (getToken()) {
    if (to.path === RouterPath.LOGIN) {
      next({ path: RouterPath.HOME });
      NProgress.done();
    } else {
      try {
        if (!store.getters.routerLoadDone) {
          const { isAdmin } = await store.dispatch("getUserInfo");
          //此处 结合vuex 动态 生成可查看的 路由地址列表
          const accessRoutes = await store.dispatch("generateRoutes", isAdmin);
          // 通过 router.addRoutes 函数，按需加载访问菜单和目录
          router.addRoutes(accessRoutes);
          next({ ...to, replace: true });
        } else {
          next();
        }
      } catch (error) {
        // remove token and go to login page to re-login
        await store.dispatch("resetToken");
        Message.error(error || "Has Error");
        next(`/login?redirect=${to.path}`);
        NProgress.done();
      }
    }
  } else {
    if (whiteList.includes(to.path)) {
      next();
    } else {
      next(RouterPath.LOGIN);
      NProgress.done();
    }
  }
});
```



#### Menus

> 路径 `src/utils/Menus.js`
>
> 思路：根据接口返回 menus 列表，返回访问的权限菜单

```javascript
import { MenuType, MenuStatus } from "@/utils/Enum/Menu";

/* 白名单 */
const whiteList = ["404"];

/**
 * 根据权限 加载菜单
 * @param {Array} assessedMenus 权限菜单
 * @param {Array} asyncRoutes 预加载菜单
 */
export function LoadMenus(assessedMenus, asyncRoutes) {
  const { CATALOG } = MenuType;
  const { ENABLE } = MenuStatus;

  /** 先预加载 需要添加的菜单 */
  let menuCache = {},
    childCache = {};
  asyncRoutes.forEach((item) => {
    const { meta, children } = item;
    /* 目录 */
    if (meta) menuCache[meta.title] = item;
    /* 菜单 */
    if (children) childsForMap(childCache, children, meta.title);
  });

  /* 需要加载的目录 */
  let resultArray = [];

  assessedMenus
    .sort((a, b) => a.orderNo - b.orderNo)
    .forEach((item) => {
      const { menuType, menuPerms, subMenus, status } = item;
      /* 目录 */
      if (+menuType === CATALOG && +status === ENABLE && menuCache[menuPerms]) {
        menuCache[menuPerms].children = [];
        menuCache[menuPerms].children = getChildMenus(
          subMenus,
          childCache,
          menuPerms
        );
        resultArray.push(menuCache[menuPerms]);
      }
    });

  /* 添加白名单 */
  whiteList.forEach((white) => {
    if (menuCache[white]) resultArray.push(menuCache[white]);
  });

  return resultArray;
}

/**
 * 获取子菜单列表
 * @param {Array} subMenus 权限子菜单
 * @param {Object} asyncChildMap 预加载子菜单 Map
 * @param {String} pCode 父级权限标识
 */
function getChildMenus(subMenus, childCache, pCode) {
  const { MENU } = MenuType;
  const { ENABLE } = MenuStatus;

  /* 子菜单集合 */
  let arr = [];
  subMenus
    .sort((a, b) => a.orderNo - b.orderNo)
    .forEach((item) => {
      const { menuPerms, menuType, status } = item;
      const _childMenu = childCache[menuPerms];
      if (+menuType === MENU && +status === ENABLE && _childMenu)
        arr.push(childCache[menuPerms]);
    });
  /* 添加 hidden页面 */
  if (childCache[pCode] && childCache[pCode].length > 0)
    arr.push(...childCache[pCode]);
  return arr;
}

/**
 * 子集菜单 做HashMap
 * @param {Object} cache 子集菜单HashMap
 * @param {Array} childs 预处理子集列表
 * @param {String} pCode 父级权限标识
 */
function childsForMap(cache, childs, pCode) {
  cache[pCode] = [];
  childs.forEach((item) => {
    const { hidden, meta } = item;
    /* 添加隐藏页 */
    if (hidden) {
      cache[pCode].push(item);
    } else cache[meta.title] = item;
  });
}
```

