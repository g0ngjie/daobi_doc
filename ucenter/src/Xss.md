# Xss防跨站攻击

> 优化 `v-html`指令



#### 使用方式

```javascript
...
<div v-html="value"></div>
...

import { Xss } from "@/utils/Xss";
computed: {
    value() {
      return Xss(`<script>alert()</script>`);
    }
}
```

