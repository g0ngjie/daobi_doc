# Vuex

> 目录地址：src/store



#### 目录结构

```bash
.
├── getters.js			# 需要编写
├── index.js
└── modules				# 需要编写
    ├── app.js
    ├── permission.js
    ├── settings.js
    ├── tagsView.js
    └── user.js
```



#### 配置

> 模块化，只需要关注**modules**下文件。和配置`getters`即可。
>
> 其余文件无需再更改

```javascript
const getters = {
  sidebar: (state) => state.app.sidebar,
  language: (state) => state.app.language,
  size: (state) => state.app.size,
  device: (state) => state.app.device,
  avatar: (state) => state.user.avatar,
  name: (state) => state.user.name,
  roles: (state) => state.user.roles,
  user: (state) => state.user.user,
  permission_routes: (state) => state.permission.routes,
  routerLoadDone: (state) => state.permission.routerLoadDone, //router是否已经加载完成
  visitedViews: (state) => state.tagsView.visitedViews,
  cachedViews: (state) => state.tagsView.cachedViews,
};
export default getters;
```

