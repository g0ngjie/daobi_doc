# 工具集

> 目录地址： `src/utils`
>
> 为了方便统一配置，避免重复工作，一些常用 工具类尽量放到 index.js里面，或者根据功能放到目录下
>
> 工具类文件命名方式尽量以`大驼峰`形式命名。例：`Auth.js`、 `Interceptor`等



#### Auth

> 用户Token工具

#### I18n

> 涉及国际化方面抽象出来的工具

#### Request

> 统一请求

#### Validate

> ElementUI Form 表单校验

#### Xss

> 防跨站脚本攻击

#### Enum

> 枚举
>
> 尽量避免硬编码，类似如下片段

```javascript
/**
 * localStorage
 * @enum
 */
export const Storage = {
  TOKEN: "EL-ADMIN-TOEKN" /* 用户token */,
  IS_ADMIN: "ynAdmin" /* 用户管理员权限 */,
};

...

import { Storage } from "@/utils/Enum";
localStorage.setItem(Storage.TOKEN, token);
```

