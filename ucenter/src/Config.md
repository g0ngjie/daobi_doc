# 环境变量

> 脚本启动时，加载到`node`进程中
>
> 此文件一般不做修改

#### 启动/打包

> 以 `yarn` 为例
>
> yarn dev == npm run dev
>
> <span style="color: #F56C6C">注：本地启动 prod 仅为 线上环境出现问题，本地调试作用</span>

```bash
yarn dev 		 # 本地开发环境
yarn dev:test 	 # 测试环境 本地调试
yarn dev:preview # 预发环境 本地调试
yarn prod 		 # 生产环境 本地调试 尽量不要用这个命令启动
```

> 打包

```bash
yarn build:test # 测试环境 打包
yarn build		# 生产环境 打包
```



#### 变量使用

> 直接请求config加载环境变量

```javascript
import { OSS_URL, RequestTimeout, CookieExpires, Prefix, ... } from "config";
```



#### .env

> 这个文件是所有配置文件是全局默认配置文件，不论什么环境都会加载合并。除非是**所有环境**都需要使用的时候且不需要改变。尽量不要修改它。
>
> 如果是单独私有的变量且其他环境不同时，例如代理某某同事本地的网关，这一类只需要在自己的**.env.development**里面改写就可以了。



#### 初始化本地配置文件

> 项目如果时第一次启动**npm run dev**，默认本地是没有配置文件**.env.development**的。
>
> 首次需要本地运行 **npm run cfg** 或者 **yarn cfg**初始化本地启动配置文件。
>
> **\*nux**环境如果要生成**.env.development** 可以直接 **./script/generateConfig.sh**
>
> 此文件已加入**.gitignore**，所以不会被**git**提交到仓库里面。
>
> 具体变量声明方式，再本地配置文件里面有标注。