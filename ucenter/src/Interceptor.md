# 拦截器

> 全局路由拦截
>
> 结合 **vuex** 下 `app/generateRoutes` 函数，可以控制路由级别的权限校验  
>
> 无需再修改



#### 白名单

> 通过 `whiteList` 设置不需要拦截的路由地址。例如 `/login`

```javascript
const whiteList = ['/login', ... ];
```

