# 图标

> 目录地址：src/icons



#### 阿里矢量图标地址

[https://www.iconfont.cn/](https://www.iconfont.cn/)

> svg图标下载到 icons里面



#### 使用

> `src/components/SvgIcon` 组件默认全局注册
>
> class-name 等价于 class类名
>
> icon-class 为 svg文件名

```javascript
<svg-icon icon-class="avatar" class-name="avatar" />
```



#### 压缩

> 命令 `yarn svgo` 通过 **svgo.yml** 配置文件，对`src/icons`下**svg**文件进行压缩

