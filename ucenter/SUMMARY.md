# Summary

* [概要](README.md)
### src目录

* [工具集](src/Utils.md)
* [环境变量](src/Config.md)
* [国际化](src/I18n.md)
* [拦截器](src/Interceptor.md)
* [Vuex](src/Vuex.md)
* [接口文件](src/Api.md)
* [图标](src/Icon.md)
* [Xss](src/Xss.md)
* [菜单&权限校验](src/Permission.md)