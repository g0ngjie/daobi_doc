# Summary

* [引导](README.md)

## src

* [架构](src/framework.md)
* [设计模式](src/design.md)
* [企业微信](src/wechat.md)
* [账户资料]()
  - [视图层](src/account/views.md)
  - [MVC 模式](src/account/vuex.md)
