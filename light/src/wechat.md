# 企业微信鉴权

> 开放平台地址 [https://work.weixin.qq.com/api/doc/](https://work.weixin.qq.com/api/doc/90001/90144/90545)
>
> 第三方鉴权操作基本都大同小异，**效率应用**里面没有做过多优化，代码用起来会比较复杂。后期改进方案可以参考**钉钉鉴权**的包 https://www.npmjs.com/package/dingtalk-auth。
>
> 下面只讲解具体使用流程



#### 引入JS文件

> 因为是在企业微信的容器内使用内置api功能，所以需要在**index.html**里面全局引入js文件
>
> （支持https）：[http://res.wx.qq.com/open/js/jweixin-1.2.0.js](https://res.wx.qq.com/open/js/jweixin-1.2.0.js)

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  ...
  <script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
  <!-- 企业微信授权登录使用 -->
  <script src="https://rescdn.qqmail.com/node/ww/wwopenmng/js/sso/wwLogin-1.0.0.js"></script>
</head>

<body>
  <div id="app"></div>
</body>

</html>
```



#### 微信调用工具

> 企业微信用到的Api文件和鉴权操作文件具体都放在了**src/utils/Wechat/WxUtils.js**文件
>
> 使用如下 **src/views/customer/material/index.vue**

```javascript
<script>
/* 客户资料 */
import { Customer } from "@/store/dispatch/index";
import { Storage } from "@/utils/Enum/index";
import { WeChatEnv } from "@/utils/Enum/WeChat";
import { getWeChatEnv } from "@/utils/Wechat/WxUtils";

export default {
  methods: {
    async init() {
      if (await this.validateEnv()) {
        this.bodyShow = true;
        await Customer.Material.getUserDetail();
        Customer.Material.initList();
        /* 账户信息列表 */
        Customer.Account.getAccounts();
      } else {
        this.$message({
          duration: 0,
          message: "请前往与客户的聊天窗口右侧工具栏查看",
          type: "warning"
        });
      }
    },
    /**校验当前入口环境 */
    async validateEnv() {
      const _env = await getWeChatEnv();
      console.log("当前入口环境:", _env);
      return _env != WeChatEnv.NORMAL;
    },
    /**
     * 定时器做调度，判断当前企业微信是否已经鉴权通过
     */
    dispatch() {
      this.intervalId = setInterval(() => {
        const config = localStorage.getItem(Storage.AGENT_CONFIG);
        if (!config) clearInterval(this.intervalId);
        else if (+config === 1) {
          //鉴权通过后，清除定时器
          clearInterval(this.intervalId);
          this.init();
        }
      }, 500);
    }
  },
  //需要在页面加载完成后做调度
  mounted() {
    this.dispatch();
  }
};
</script>
```

