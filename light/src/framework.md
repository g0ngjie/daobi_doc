# 架构

#### 目录结构

> 实际上和**Ucenter**基本上大致一样，有国际化、Vuex、工具集、公共组件、icons等
>
> 项目在一些设计上有一些不同于其他系统的设计，此文档只针不同点结合实例做具体分析和介绍
>
> 其他具体细节可以参考**项目搭建、规范**

```shell
.
├── babel.config.js
├── config
├── deploy.sh
├── jsconfig.json
├── package.json
├── public
├── README.md
├── script
│   ├── generateConfig
│   └── generateConfig.sh
├── src
│   ├── api
│   ├── App.vue
│   ├── assets
│   ├── components
│   ├── directives
│   ├── filters
│   ├── icons
│   ├── lang
│   ├── main.js
│   ├── mixins
│   ├── proxy
│   ├── router
│   ├── store
│   ├── styles
│   ├── utils
│   └── views
└── vue.config.js
```



#### UI框架

> **ElementUI** + **Vant**
>
> 最开始考虑用轻量级UI**(Vant)**，但编码账户详情的时候，对于这种移动端框架的样式，像**select**选择器，没有类似传统下拉选择的功能。由于视觉上的要求，最终决定整体迁移为**ElementUI**。在现有代码基础上做迁移，消耗的成本比较高。考虑到很多组件已经再使用**Vant**，所以目前来说，**效率应用**里面同时存在两种UI框架。
>
> 不过项目再编译打包时，做分片处理。一个大文件会分成若干的小片段。浏览器再加载资源的时候，异步资源请求，所以速度上面进行了一定的优化。



