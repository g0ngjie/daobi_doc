# MVC模式

> Vuex、Proxy、Api



#### Api文件

> 目录 **src/api**
>
> 后端接口请求



#### Proxy

> 目录 **src/proxy/customer/Materials.js**
>
> 用来调用 Api文件，做类似持久层数据获取

```javascript
import ExceptionBase from "@/utils/ExceptionBase";
import {
  ucStudentQueryOne,
  ucStudentSave,
  ucStudentQueryTag,
  ucStudentAddTag,
} from "@/api/user";

class Materials extends ExceptionBase {
  /**
   * 根据用户wxId查询用户资料
   * @param {string} wxUserId 企业微信用户id
   */
  async getUserDetailByWxId(wxUserId) {
    const { ok, body } = await this.catch(ucStudentQueryOne, wxUserId);
    return ok ? body : {};
  }

  /**
   * 修改用户资料
   * @param {Object} form 表单
   */
  async updateUserDetail(form) {
    const { ok } = await this.catch(ucStudentSave, form);
    return ok;
  }

  /**
   * 获取用户标签列表
   * @param {string} wxId 企业微信用户id
   */
  async getUserTags(wxId) {
    const { ok, body } = await this.catch(ucStudentQueryTag, { wxId });
    if (ok) return body || [];
    return [];
  }

  /**
   * 更新/添加用户标签
   * @param {string} wxId
   * @param {Array<Object>} tags 标签列表
   */
  async updateUserTags(wxId, tags) {
    const { ok } = await this.catch(ucStudentAddTag, {
      wxId,
      tags,
    });
    return ok;
  }
}

export default new Materials();
```



#### Store

> 目录 **src/store/modules/customer_material.js**
>
> Vuex



#### Dispatch

> 目录 **src/store/dispatch/index.js**
> 视图层统一做接口分发

```javascript
import Material from "./customer/material";
import Account from "./customer/account";

/**
 * 客户
 */
export const Customer = {
  /**
   * 用户详情
   */
  Material,
  /**
   * 账户信息
   */
  Account,
};
```



```javascript
import Store from "@/store/index";

export default {
  /**查询账户信息列表 */
  getAccounts: () => Store.dispatch("customer_material_account/getAccounts"),
  /**
   * 绑定用户-查询业务系统客户列表信息
   * @param {string} sysCode db|wx
   * @param {string} key 查询信息
   */
  getBindUserList: ({ sysCode, key }) =>
    Store.dispatch("customer_material_account/getBindUserList", {
      sysCode,
      key,
    }),

  /**
   * 绑定用户
   * @param {number} childType 1 文亚  2导毕
   * @param {number} ucId
   * @param {string} userId 子系统学生ID
   * @param {string} bindType 1绑定  2创建   如果选择创建 则不需要选择childType
   */
  bindUser: ({ childType = null, userId = null, bindType }) =>
    Store.dispatch("customer_material_account/bindUser", {
      childType,
      userId,
      bindType,
    }),
  /**查询币种列表 */
  getCurrencyList: () =>
    Store.dispatch("customer_material_account/getCurrencyList"),
  /**
   * 查询历史充值记录
   * @param {string} sysCode db|wx
   * @param {string} userId 用户id
   */
  getHistoryRecharge: (sysCode, userId) =>
    Store.dispatch("customer_material_account/getHistoryRecharge", {
      sysCode,
      userId,
    }),
  /**
   * 充值
   * @param {string} current
   * @param {number} money
   * @param {string} thumbUrl
   */
  recharge: (form) =>
    Store.dispatch("customer_material_account/recharge", form),
  /**
   * 学生账户更新
   */
  updateAccount: (sysCode) =>
    Store.dispatch("customer_material_account/updateAccount", sysCode),

  /**
   * 导毕|文亚 账户充值
   * @param {string} sysCode
   * @param {Object} data
   */
  createAccount: (sysCode, data) =>
    Store.dispatch("customer_material_account/createAccount", {
      sysCode,
      data,
    }),

  /**
   * 导毕|文亚 转汇
   * @param {string} sysCode
   * @param {Object} data
   */
  changeAccount: (sysCode, data) =>
    Store.dispatch("customer_material_account/changeAccount", {
      sysCode,
      data,
    }),
};
```

