# 视图层

> 账户信息下视图表现层
>
> Views下只做Dom元素的展示和交互



#### 目录

> src/views/customer/material



#### Views结合Vuex

> views下数据通过 Store下的state获取，取代 vue里的 $data
>
> vue里 $data 只做Dom层交互或当前页面form表单的cache

```javascript
<script>
/* 客户资料 */

import { mapState } from "vuex";
import { Gender2Css, GradeList } from "./Cfg";
import { validatePhone } from "@/utils/Validate";
import { Customer } from "@/store/dispatch/index";
import { Storage } from "@/utils/Enum/index";

export default {
  components: { fieldTemp, selectTemp, tabsTemp, tagsTemp },
  data() {
    return {
      rulePhone: { trigger: "change", validator: validatePhone },
      ruleEmail: { trigger: "change", type: "email", message: "邮箱格式错误" },
      isExpend: false,
      Gender2Css,
      GradeList,
      intervalId: null,
      WxGendor,
      bodyShow: false
    };
  },
  computed: {
    ...mapState({
      form: e => e.customer_material.user,
      countryList: e => e.customer_material.countryList,
      schoolList: e => e.customer_material.schoolList,
      majorList: e => e.customer_material.majorList,
      eduList: e => e.customer_material.eduList,
      loading: e => e.customer_material.loading
    })
  },
  methods: {
    /* 根据国家id获取学校列表 */
    async getSchoolByCountryId(id) {
      this.form.universityId = null;
      await Customer.Material.getSchoolListByCountryId(id);
    },
  }
};
</script>
```

