# 阶段配置文件

> 目录 **src/views/dissertation/cfg/Stage.js**

```javascript
import { StageField, StageStatus, StageType } from "@/utils/VEnum";

const { ORDER, PHASE, CONTAIN, ALREADY, SUMMARY } = StageField;

const {
    BREAKING_ICE,
    FIXED_TOPIC,
    INTRODUCTION,
    REVIEW,
    METHODOLOGY,
    ANALYSIS,
    CONCLUSION,
    EXAMINE
} = StageType;

export const StageType2Val = {
    [BREAKING_ICE]: "阶段一",
    [FIXED_TOPIC]: "阶段二",
    [INTRODUCTION]: "阶段三",
    [REVIEW]: "阶段四",
    [METHODOLOGY]: "阶段五",
    [ANALYSIS]: "阶段六",
    [CONCLUSION]: "阶段七",
    [EXAMINE]: "阶段八"
};

export const StageDatas = [
    {
        [ORDER]: "阶段一",
        [PHASE]: "Introduction",
        [CONTAIN]: "论文框架搭建、绪论部分批改课",
        [ALREADY]: "130mins",
        [SUMMARY]: "因学生自身原因阶段延误"
    },
    {
        [ORDER]: "阶段二",
        [PHASE]: "Literature Review",
        [CONTAIN]: "LR课、LR精读课、Tutor Meeting课-LR01、LR部分批",
        [ALREADY]: "0mins",
        [SUMMARY]: ""
    },
    {
        [ORDER]: "阶段三",
        [PHASE]: "Literature Review",
        [CONTAIN]:
            "LR课、LR精读课、Tutor Meeting课-LR01、LR部分批LR课、LR精读课、Tutor Meeting课-LR01、LR部分批",
        [ALREADY]: "0mins",
        [SUMMARY]: ""
    },
    {
        [ORDER]: "阶段四",
        [PHASE]: "Methodology",
        [CONTAIN]: "解锁研究方法、Tutor Meeting课、ME部分批改课",
        [ALREADY]: "0mins",
        [SUMMARY]: ""
    },
    {
        [ORDER]: "阶段五",
        [PHASE]: "Data Analysis",
        [CONTAIN]: "数据处理结果剖析课、Tutor Meeting课",
        [ALREADY]: "0mins",
        [SUMMARY]: ""
    },
    {
        [ORDER]: "阶段六",
        [PHASE]: "Dicussion&Conlusion",
        [CONTAIN]: "讨论总结课、D&C部分批改课",
        [ALREADY]: "0mins",
        [SUMMARY]: "sadfasdkljsdhfkjahdl1111252551111111"
    },
    {
        [ORDER]: "阶段七",
        [PHASE]: "梳理全文",
        [CONTAIN]: "Tutor Meeting课、全文校验",
        [ALREADY]: "0mins",
        [SUMMARY]:
            "这撒封口机啊的罚款加快速度激发快速减肥开设的级上看到激发开设的激发看到街上"
    }
];

const { DONE, DOING } = StageStatus;

/* 阶段顺序 状态 图片 */
export const OrderImg = {
    [DONE]: require("@/assets/images/dissertation/stage/done.png"),
    [DOING]: require("@/assets/images/dissertation/stage/doing.png")
    // [NEW]: require("@/assets/images/dissertation/stage/new.png")
};
```

