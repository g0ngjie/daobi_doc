# 枚举

> 目录 **src/utils/VEnum.js**

```javascript
/**
 * 毕业论文大礼包 路由路径
 * @readonly
 * @enum {Number}
 */
export const DissertationRouterPath = {
    HOME: "/dissertation" /* 首页 */
};

/**
 * 阶段 字段枚举
 */
export const StageField = {
    ORDER: "order" /* 阶段顺序 */,
    PHASE: "phase" /* 课程阶段 */,
    CONTAIN: "contain" /* 包含课程 */,
    ALREADY: "already" /* 已上课时长 */,
    SUMMARY: "summary" /* 阶段总结 */
};

/**
 * 阶段状态  0未开始  1正在进行 2已完成
 */
export const StageStatus = {
    TODO: 0 /* 0未开始 */,
    DOING: 1 /* 1正在进行 */,
    DONE: 2 /* 2已完成 */
};

/**
 * 阶段类型枚举
 */
export const StageType = {
    BREAKING_ICE: 0 /* 1 破冰阶段 */,
    FIXED_TOPIC: 1 /* 1 定题阶段 */,
    INTRODUCTION: 2 /* 2 introduction */,
    REVIEW: 3 /* 3 Literature Review  */,
    METHODOLOGY: 4 /* 4 Methodology */,
    ANALYSIS: 5 /* 5 Data Analysis */,
    CONCLUSION: 6 /* 6 Dicussion&Conclusion */,
    EXAMINE: 7 /* 7 梳理全文 */
};

/**
 * 课堂状态 枚举（进行中，已完成，未完成）
 * 状态 课堂说明 0未开启 1待排课 2待上课 3已完成
 * @enum
 */
export const ClassStatus = {
    TODO: 0 /* 0未开启 */,
    WAIT: 1 /* 1待排课 */,
    BEGIN: 2 /* 2待上课 */,
    DONE: 3 /* 3已完成 */
};

/**
 * 课程类别 是否是加课 0否 1是
 * @enum
 */
export const AddClasses = {
    NO: 0 /* 0否 */,
    YES: 1 /* 1是 */
};

/**
 * 课程类别
 * 阶段课类型 1必须上课  2可选上课  3 必论文修改
 * @enum
 */
export const StageClassesType = {
    MUST: 1 /* 1必须上课 */,
    SELECTABLE: 2 /* 2可选上课 */,
    DISSERTATION: 3 /* 3 必论文修改 */
};

/**
 * Table Event
 * @enum
 */
export const TableEvent = {
    LOOK_UP: 1 /* 查看 */,
    UPLOAD: 2 /* 上传 */,
    EDIT: 3 /* 修改 */,
    ARRANGE: 4 /* 排课 */
};

/**
 * Stage Event
 */
export const StageEvent = {
    MODIFIED: 1 /* 修改 */,
    FEEDBACK: 2 /* 反馈 */
};

/**
 * 讲师材料 stageClassFileType
 * 阶段课堂文件类型
 * 1topicList  2 outLine  3 批改文件
 * 4 data Results 5 Final Document
 * 6 Research List 7 批改文件完整
 * @enum
 */
export const StageClassFileType = {
    TOPIC_LIST: 1 /* 1topicList */,
    OUTLINE: 2 /* 2 outLine */,
    CORRECTION: 3 /* 3 批改文件 */,
    DATA_RESULTS: 4 /* 4 data Results */,
    FINAL_DOC: 5 /* 5 Final Document */,
    RESEARCH_LIST: 6 /* 6 Research List */,
    COMPLETE: 7 /* 7 批改文件完整 */
};

/**
 * 课堂上传文件类型
 * 1 毕业大论文订单课堂讲师材料 2 毕业大论文创建引流订单 学生文件 3 毕业大论文创建收费订单 学生近期自主完成的essay 4 毕业大论文阶段信息 学生资料 5 毕业大论文阶段必论文修改 上传文件
 * @enum
 */
export const ClassUploadFileType = {
    TEACHER_MATERIAL: 1 /* 毕业大论文订单课堂讲师材料 */,
    MUST_MATERIAL: 5 /* 毕业大论文阶段必论文修改 */
};

/**
 * 当前大论文订单进展状态
 * @enum
 */
export const HeaderStatused = {
    DONE: 3 /* 已完成 */,
    OVER: 4 /* 已结课 */,
    ERR_OVER: 5 /* 异常结课 */
};

/**
 * crm 跳转规划表 参数hashkey 枚举
 * @enum
 */
export const CrmRouterLinkArgs = {
    TOKEN: "Q7HbyVkEtAGTKBk5",
    COURSE_ID: "b7L3ZNXIKy42qV39",
    PATH: "5AOriMixcF9r5s50"
};
```

