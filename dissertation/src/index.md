> 目录 **src/views/dissertation/index.vue**

```javascript
<template>
  <!-- 毕业论文大礼包 -->
  <div class="dissertation-container">
    <!-- title -->
    <G-Title></G-Title>
    <!-- 阶段 -->
    <G-Stage></G-Stage>
    <!-- 表格 -->
    <G-Table></G-Table>
    <!-- modal -->
    <Adjust-Class-Modal ref="adjustModal"></Adjust-Class-Modal>
  </div>
</template>

<script>
import { GTitle, GStage, GTable } from "./components"; // title表头 stage 阶段组件 table 表格组件
import { StageType, ClassStatus } from "@/utils/VEnum"; //枚举类型引入
import { AdjustClassModal } from "@/views/dissertation/components/Modal"; //提示弹窗 -> 调整课堂阶段
import { mapGetters } from "vuex";

export default {
  components: { //组件注册
    GTitle,
    GStage,
    GTable,
    AdjustClassModal // 提示弹窗注册
  },
  computed: {
    ...mapGetters(["stageTables"])// initList 后 获取 table表数据列表
  },
  methods: {
    /* 判断是否需要 调整弹窗 */
    isAdjust() {
      const { stageClassInstance, stageType } = this.stageTables[1];
      if (+stageType === StageType.FIXED_TOPIC) {
        const { statused } = stageClassInstance[0];
        if (+statused === ClassStatus.WAIT) this.$refs.adjustModal.show();
      }
    },
    /* 初始化列表，获取规划表数据*/
    async initList() {
      await this.$store.dispatch("dissertation/reload");
      await this.$store.dispatch("dissertation/initTitle");
      this.isAdjust();
    }
  },
  mounted() {
    this.initList();
  }
};
</script>
```

