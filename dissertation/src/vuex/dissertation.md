# 规划表 Vuex

> 目录 **src/store/modules/dissertation.js**

```javascript
import {
    paper_stage,
    paper_teacherRate,
    crmStageTable,
    crmTeacherRate
} from "@/views/dissertation/api";
import { DissertationRouterPath, Storage, HeaderStatused } from "@/utils/VEnum";
import _ from "lodash";
import Router from "@/router";

function cacheIdAndStatus({ courseId, readonly }) {
    localStorage.removeItem(Storage.COURSE_ID);
    localStorage.removeItem(Storage.READ_ONLY);
    localStorage.setItem(Storage.COURSE_ID, courseId);
    localStorage.setItem(Storage.READ_ONLY, readonly);
}

function getReadonly() {
    const readonly = JSON.parse(localStorage.getItem(Storage.READ_ONLY));
    return readonly;
}
function getCourseId() {
    const courseId = JSON.parse(localStorage.getItem(Storage.COURSE_ID));
    return courseId;
}

const state = {
    disabled: false /* 拖拽组件 禁用 */,
    tables: [] /* 表格组件数据 */,
    stages: [] /* 阶段组件数据 */,
    titleData: {} /* 标题组件数据 */,
    rate: null /* 有值已经评级，空则没有评级 */,
    stageLoading: false,
    progressOver: false /* 总进展是否已经结束 */,
    courseId: getCourseId() /* 订单id */,
    readonly: getReadonly() /* 不允许操作 */
};

const mutations = {
    DISABLED: (state, bool) => {
        state.disabled = bool;
    },
    UPDATE_STAGES: (state, list) => {
        state.stages = list;
    }
};

const actions = {
    setDisabled({ commit }, bool) {
        commit("DISABLED", bool);
    },
    /**数据重新获取，并cache */
    async reload({ state }) {
        state.stageLoading = true;
        let body, status;
        const isCrm = localStorage.getItem(Storage.CRM_LINK_DISS);
        if (isCrm) {
            const res = await crmStageTable({ courseId: state.courseId });
            status = res.status;
            body = res.body;
        } else {
            const res = await paper_stage({ id: state.courseId });
            status = res.status;
            body = res.body;
        }
        state.stageLoading = false;
        if (status === 200) {
            state.tables = body;
            state.stages = body.map(item => {
                return {
                    ...item,
                    contain: item.stageClass
                        .map(stage => stage.className)
                        .join("、")
                };
            });
        }
    },
    /** 规划表 title数据获取，并cache */
    async initTitle({ state }) {
        let body, status;
        const isCrm = localStorage.getItem(Storage.CRM_LINK_DISS);
        if (isCrm) {
            const res = await crmTeacherRate({ courseId: state.courseId });
            status = res.status;
            body = res.body;
        } else {
            const res = await paper_teacherRate({
                courseId: state.courseId
            });
            status = res.status;
            body = res.body;
        }
        if (status === 200) {
            const { OVER, ERR_OVER } = HeaderStatused;
            const {
                schedule,
                dealDeadline,
                officialDeadline,
                statused,
                rate
            } = body;
            state.titleData = {
                schedule: Math.round(+schedule * 100),
                dealDeadline,
                officialDeadline,
                statused,
                rate
            };
            state.progressOver = [OVER, ERR_OVER].includes(+statused);
            state.rate = rate;
        }
    },
   	/** 跳转到规划表 */
    async gotoDissertation({ state }, data) {
        const { courseId, readonly } = data;
        if (_.isNil(courseId) || _.isNil(readonly))
            throw "courseId or readonly is Nil";
        cacheIdAndStatus({
            courseId,
            readonly
        });
        const __router = Router.resolve({
            path: DissertationRouterPath.HOME
        });
        window.open(__router.href, "_blank");
    },
    /* crm系统跳转进入规划表 */
    async crmGotoDissertation({ state }, data) {
        const { courseId } = data;
        if (_.isNil(courseId)) throw "courseId is Nil";
        cacheIdAndStatus({
            courseId,
            readonly: true
        });
        Router.push(DissertationRouterPath.HOME);
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};
```

