# state

> 目录 

```javascript
const getters = {
    draggableDisabled: state => state.dissertation.disabled,/* 拖拽组件 禁用 */
    stages: state => state.dissertation.stages,/* 阶段组件数据 */
    stageTables: state => state.dissertation.tables,/* 表格组件数据 */
    stageLoading: state => state.dissertation.stageLoading,/* 阶段loading状态 */
    courseId: state => state.dissertation.courseId,/* 订单id */
    dissertationTitleData: state => state.dissertation.titleData,/* 标题组件数据 */
    progressOver: state => state.dissertation.progressOver,/* 总进展是否已经结束 */
    dissertationRate: state => state.dissertation.rate,/* 有值已经评级，空则没有评级 */
    readonly: state => state.dissertation.readonly,/* 只读状态 */
};
export default getters;
```

