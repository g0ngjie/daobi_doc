# Title组件

> title展示组件，没有过多解锁，代码结构比较清晰

```javascript
<template>
  <div class="title-container">
    <div class="title">
      <span>毕业论文规划表</span>
    </div>
    <div class="row">
      <!-- left -->
      <section class="left">
        <div>
          <span>Deal Deadline:&ensp;</span>
          <span>{{dissertationTitleData.dealDeadline | fmtDate}}</span>
        </div>
        <div>
          <span>Offical Deadline:&ensp;</span>
          <span>{{dissertationTitleData.officialDeadline | fmtDate}}</span>
        </div>
        <div class="progressbar-container">
          <span>毕业论文辅导进度:&ensp;</span>
          <div class="progressbar">
            <div class="process">
              <div class="percent" :style="`width: ${dissertationTitleData.schedule}%`"></div>
            </div>
            <span>{{dissertationTitleData.schedule}}%</span>
          </div>
        </div>
      </section>
      <!-- right -->
      <section class="right">
        <div class="btn" v-if="+dissertationTitleData.statused >= 3" @click="openModal">论文评级</div>
        <div class="btn disabled" v-else>论文评级</div>
      </section>
    </div>

    <!-- modal -->
    <section>
      <Grade-Modal ref="gradeModal"></Grade-Modal>
    </section>
  </div>
</template>

<script>
import { GradeModal } from "@/views/dissertation/components/Modal";
import moment from "moment";
import { mapGetters } from "vuex";

export default {
  components: { GradeModal },
  filters: {
    fmtDate(val) {
      if (val) return moment(val).format("YYYY-MM-DD");
    }
  },
  computed: {
    ...mapGetters(["dissertationTitleData", "progressOver"])
  },
  methods: {
    openModal() {
      this.$refs.gradeModal.show();
    }
  }
};
</script>
```

