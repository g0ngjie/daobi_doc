# 标题栏

> 目录 **src/views/dissertation/components/Table/Header.vue**

```javascript
<template>
  <div class="g-table-header-row header">
    <div class="phase">
      <span>课程阶段</span>
    </div>
    <div class="name">
      <span>课堂名称</span>
    </div>
    <div class="type">
      <span>课堂类别</span>
    </div>
    <div class="plan">
      <span>安排说明</span>
    </div>
    <div class="time">
      <span>课程时间</span>
    </div>
    <div class="classExplain">
      <span>课堂状态</span>
    </div>
    <div class="classroomRemark">
      <span>课堂说明</span>
    </div>
    <div class="timeRange">
      <span>上课时长</span>
    </div>
    <div class="material">
      <span>讲师材料</span>
    </div>
    <div class="planClass">
      <span>排课</span>
    </div>
  </div>
</template>
```

