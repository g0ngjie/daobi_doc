# 表格

> 目录 **src/views/dissertation/components/Table/Table.vue**

```javascript
<template>
  <div class="g-table-container">
    <!-- table row -->
    <section class="g-table-row">
      <!-- left -->
      <div class="phase">
        <i v-if="expand" class="icon el-icon-arrow-up" @click="expand = false"></i>
        <i v-else class="icon el-icon-arrow-down" @click="expand = true"></i>
        <div class="btn-box">
          <el-tooltip :content="StageType2Val[table.stageType]" placement="top">
            <span>{{StageType2Val[table.stageType] | fmtSpan }}</span>
          </el-tooltip>
          <div class="btn readonly" v-if="readonly">加课</div>
          <div v-else>
            <div
              class="btn"
              v-if="!progressOver && +table.statused !== StageStatus.TODO"
              @click="openAdd"
            >加课</div>
            <div class="btn readonly" v-else>加课</div>
          </div>
        </div>
      </div>
      <!-- right -->
      <section class="g-right-rows">
        <G-Right-Row
          v-show="expand"
          :stageType="table.stageType"
          :stageId="table.id"
          v-for="(item, i) in table.stageClassInstance"
          :key="i"
          :data="item"
        ></G-Right-Row>
        <G-Right-Row
          v-show="!expand"
          :stageType="table.stageType"
          :stageId="table.id"
          :data="table.stageClassInstance[0]"
        ></G-Right-Row>
        <div v-show="!expand" class="retract">... ...</div>
      </section>
    </section>
    <!-- modal -->
    <section>
      <Add-Class-Modal ref="addClassModal"></Add-Class-Modal>
    </section>
  </div>
</template>

<script>
import GRightRow from "./RightRow";
import { StageType2Val } from "@/views/dissertation/cfg/Table";
import { AddClassModal } from "@/views/dissertation/components/Modal";
import { mapGetters } from "vuex";
import { StageStatus } from "@/utils/VEnum";

export default {
  components: {
    GRightRow,
    AddClassModal
  },
  filters: {
    fmtSpan(val) {
      const _index = val.indexOf("&");
      if (_index > 0) {
        return `${val.substr(0, _index + 1)}\n${val.substr(_index + 1)}`;
      }
      return val;
    }
  },
  computed: {
    ...mapGetters(["readonly", "progressOver"])
  },
  props: {
    table: {
      type: Object,
      default() {}
    }
  },
  data() {
    return {
      StageStatus,
      StageType2Val,
      expand: true /* 课程阶段展开 */
    };
  },
  methods: {
    openAdd() {
      this.$refs.addClassModal.show(this.table.stageType);
    }
  }
};
</script>
```

