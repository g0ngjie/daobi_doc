# 添加课堂

> 目录 **src/views/dissertation/components/Modal/AddClass.vue**

```javascript
<template>
  <!-- 添加课堂 -->
  <div class="add-class-container">
    <el-dialog :visible.sync="isShow" center width="600px">
      <template slot="title">
        <div class="title-box">
          <span class="q-title">添加课堂</span>
        </div>
      </template>
      <div v-loading="loading">
        <div class="add-checkbox">
          <div class="label">
            <span>{{title}}</span>
            <span>阶段课堂</span>
          </div>
          <div class="content-list">
            <div
              :class="{ active: isActive === i }"
              v-for="(item, i) in classTypes"
              :key="i"
              @click="checkClassType(item, i)"
            >
              <span>{{item.label}}</span>
            </div>
          </div>
        </div>
        <div class="upload-form">
          <div>
            <div class="label">
              <img :src="require('@/assets/images/dissertation/modal/name.png')" />
              <span>课堂名称</span>
            </div>
            <div class="content">
              <el-input size="mini" disabled v-model="form.className" placeholder="请输入课堂名称"></el-input>
            </div>
          </div>
          <div>
            <div class="label">
              <img :src="require('@/assets/images/dissertation/modal/remark.png')" />
              <span>课堂说明</span>
            </div>
            <div class="content">
              <el-input
                size="mini"
                :maxlength="255"
                v-model="form.remark"
                type="textarea"
                placeholder="填写课堂说明"
              ></el-input>
            </div>
          </div>
          <div class="btn-box">
            <div class="btn-plain" @click="isShow = false">取消</div>
            <div class="btn" @click="validate">确定添加</div>
          </div>
        </div>
      </div>
    </el-dialog>
  </div>
</template>

<script>
import {
  StageType2Val,
  StageType2ClassTypeListVal
} from "@/views/dissertation/cfg/Table";
import { add_class } from "@/views/dissertation/api";

export default {
  data() {
    return {
      isShow: false,
      loading: false,
      title: "",
      form: {},
      isActive: 0,
      classTypes: []
    };
  },
  methods: {
    show(stageType) {
      this.form = {};
      const __classTypes = StageType2ClassTypeListVal[stageType];
      const { classType, label } = __classTypes[0];
      this.form = {
        stageType,
        classType,
        className: label
      };
      this.classTypes = __classTypes;
      this.title = StageType2Val[stageType];
      this.isShow = true;
    },
    checkClassType(data, index) {
      const { classType, label } = data;
      this.isActive = index;
      this.form.classType = classType;
      this.form.className = label;
    },
    validate() {
      const { className } = this.form;
      if (!className || !className.trim())
        return this.$message.warning("请输入课堂名称");
      this.submit();
    },
    async submit() {
      this.loading = true;
      const { status, body } = await add_class({
        courseId: this.$store.getters.courseId,
        ...this.form
      });
      this.loading = false;
      if (status === 200) {
        this.$message.success("添加成功");
        this.isShow = false;
        await this.$store.dispatch("dissertation/reload");
        await this.$store.dispatch("dissertation/initTitle");
      }
    }
  }
};
</script>
```

