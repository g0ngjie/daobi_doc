# 论文评级

> 目录 **src/views/dissertation/components/Modal/Grade.vue**

```javascript
<template>
  <!-- 论文评级 -->
  <div class="dissertation-grade-container">
    <el-dialog :visible.sync="isShow" center width="600px">
      <template slot="title">
        <div class="title-box">
          <span class="q-title">论文评级</span>
        </div>
      </template>
      <div class="grade-form">
        <div class="tips">
          <span>请针对学生的论文水平，给出你客观的评价等级不能修改</span>
        </div>
        <div class="check-box">
          <div>
            <img
              :class="{ isActive: form.rate === 1 }"
              @click="checkGrade(1)"
              :src="require('@/assets/images/dissertation/modal/A.png')"
            />
            <span>优秀</span>
            <span class="remark">100-75分</span>
          </div>
          <div>
            <img
              :class="{ isActive: form.rate === 2 }"
              @click="checkGrade(2)"
              :src="require('@/assets/images/dissertation/modal/B.png')"
            />
            <span>良好</span>
            <span class="remark">55-74分</span>
          </div>
          <div>
            <img
              :class="{ isActive: form.rate === 3 }"
              @click="checkGrade(3)"
              :src="require('@/assets/images/dissertation/modal/C.png')"
            />
            <span>一般</span>
            <span class="remark">45-54分</span>
            <span class="error warning">存在挂科风险</span>
          </div>
          <div>
            <img
              :class="{ isActive: form.rate === 4 }"
              @click="checkGrade(4)"
              :src="require('@/assets/images/dissertation/modal/D.png')"
            />
            <span class="error">必挂</span>
            <span class="error">小于45分</span>
          </div>
        </div>
        <div class="btn-box" v-if="!dissertationRate">
          <div class="btn-plain" @click="isShow = false">取消</div>
          <div class="btn" @click="submit">确定</div>
        </div>
      </div>
    </el-dialog>
  </div>
</template>

<script>
import { paper_rate } from "@/views/dissertation/api";
import { mapGetters } from "vuex";

export default {
  data() {
    return {
      isShow: false,
      form: {}
    };
  },
  computed: {
    ...mapGetters(["dissertationRate"])
  },
  methods: {
    show() {
      this.form = { rate: this.dissertationRate || 1 };
      this.isShow = true;
    },
    checkGrade(check) {
      if (this.dissertationRate) return;
      this.form.rate = check;
    },
    async submit() {
      const { status, body } = await paper_rate({
        courseId: this.$store.getters.courseId,
        rate: this.form.rate
      });
      if (status === 200) {
        await this.$store.dispatch("dissertation/initTitle");
        this.isShow = false;
        this.$message.success("操作成功");
      }
    }
  }
};
</script>
```

