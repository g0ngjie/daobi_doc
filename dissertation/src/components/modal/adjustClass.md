# 调整课堂

> 目录 **src/views/dissertation/components/Modal/AdjustClass.vue**

```javascript
<template>
  <!-- 调整课堂弹窗 -->
  <div class="adjust-class-container">
    <el-dialog :visible.sync="isShow" center width="500px">
      <template slot="title">
        <div class="title-box">
          <span class="q-title">调整课堂阶段</span>
        </div>
      </template>
      <div class="content">
        <span>请根据学生需求调整相应上课的阶段顺序</span>
      </div>
      <div class="btn-box">
        <div class="btn i-know" @click="isShow = false">我知道了</div>
      </div>
    </el-dialog>
  </div>
</template>

<script>
export default {
  data() {
    return {
      isShow: false
    };
  },
  methods: {
    show() {
      this.isShow = true;
    }
  }
};
</script>
```

