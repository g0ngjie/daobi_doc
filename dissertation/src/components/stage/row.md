# 行组件

> 目录 **src/views/dissertation/components/Stage/Row.vue**

```javascript
<template>
  <!-- Stage Row -->
  <div class="stage-container">
    <!-- 阶段 header -->
    <div class="stage-row header" :class="{stripe: index%2 === 0}" v-if="isHeader">
      <div class="order">阶段顺序</div>
      <!-- 阶段名称 -->
      <div class="phase">课程阶段</div>
      <div class="contain">包含课程</div>
      <div class="already">已上课时长</div>
      <div class="summary">
        阶段总结
        <span>请针对学生该阶段的表现予以评价(仅师资可见)</span>
      </div>
    </div>
    <!-- 阶段 row -->
    <div class="stage-row" :class="{hover: true, stripe: index%2 !== 0, 'last-row': isLast}" v-else>
      <div class="order">
        <img :src="OrderImg[+data.statused]" />
        <span>{{StageType2Val[data.sort - 1]}}</span>
      </div>
      <div class="phase">{{data.stageName}}</div>
      <div class="contain">
        <el-tooltip v-if="overflow(data.contain)" :content="data.contain" placement="top">
          <span class="span-limit">{{data.contain}}</span>
        </el-tooltip>
        <span v-else>{{data.contain}}</span>
      </div>
      <div class="already">{{data.classTime}}mins</div>
      <div class="summary">
        <G-Summary :data="data"></G-Summary>
      </div>
    </div>
  </div>
</template>

<script>
import GSummary from "./Summary";
import { OrderImg, StageType2Val } from "@/views/dissertation/cfg/Stage";
import { getChars } from "@/utils/Dissertation";

export default {
  components: { GSummary },
  data() {
    return {
      OrderImg,
      StageType2Val
    };
  },
  props: {
    isHeader: {
      type: Boolean,
      default: false
    },
    data: {
      type: Object,
      default() {}
    },
    index: {
      type: Number,
      default: 0
    },
    isLast: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    overflow(strings) {
      const MAX = 67;
      const len = getChars(strings);
      return len >= MAX;
    }
  }
};
</script>
```

