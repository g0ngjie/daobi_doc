> @classbro-teacher-web-vue
>
> 规划表结合源码做具体分析

```shell
.
├── api					#接口文件
│   ├── index.js		#api主文件
│   └── mock.js			#mock数据
├── cfg					#配置文件
│   ├── Stage.js		#阶段配置文件
│   └── Table.js		#表格配置文件
├── components			#组件目录
│   ├── index.js		#主文件
│   ├── Modal			#弹窗目录
│   ├── Stage			#阶段组件
│   ├── Table			#表格组件
│   └── Title.vue		#title
└── index.vue			#主页
```

