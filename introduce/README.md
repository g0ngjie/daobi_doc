#### 技术选型

> 部分模块由于官方或vue-element-admin作者已经完善的很好了。我不过多修改，直接挪用。

> 项目基于 vue-cli@3、webpack@4、element-ui@2.12.0、scss、

- **ECMAScript** 的新标准 
- **vue-cli** 最新
- **webpack** 跟随最新cli
- [element-ui](https://element.eleme.cn/#/zh-CN)
- scss
  - element-ui 底层使用的是scss。




