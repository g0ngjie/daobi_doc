# Summary

* [引导](README.md)
### src目录

* [Vscode](src/vscode.md)
* [Jsdoc](src/jsdoc.md)
* [Webpack](src/webpack.md)
* [路由和侧边栏](src/router.md)
* [环境变量](src/env.md)
* [跨域问题](src/cors.md)
* [路由懒加载](src/router-lazyload.md)
* [图标](src/icons.md)
* [国际化](src/i18n.md)
* [ESLint](src/eslint.md)
* [权限验证](src/permission.md)
* [样式](src/style.md)

