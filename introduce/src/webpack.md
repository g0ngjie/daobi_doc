# Webpack相关配置

> 编译、打包、配置环境变量
>
> 详细文档查阅 https://cli.vuejs.org/zh/guide/mode-and-env.html



#### 配置文件

> .env 全局变量
>
> .env.development 本地开发是用到的变量，默认没有此文件
>
> .env.preview 预发布环境
>
> .env.testing 测试环境



#### .env.development

> 此文件不跟随git提交。已经添加到 ignore文件里面。
>
> 本地第一次启动 需要运行脚本 目录在 **script/generateConfig.sh**
>
> 通过命令 `yarn cfg` 或 `./script/generateConfig.sh` 两者都可以



#### vue.config.js

> webpack 分片

```javascript
config.optimization.splitChunks({
      chunks: "all",
      minSize: 30000, //字节 引入的文件大于30kb才进行分割
      maxSize: 50000, //50kb，尝试将大于50kb的文件拆分成n个50kb的文件
      cacheGroups: {
        libs: {
          name: "chunk-libs",
          test: /[\\/]node_modules[\\/]/,
          priority: 10,
          chunks: "initial", // only package third parties that are initially dependent
        },
        vant: {
          name: "chunk-vant", // split elementUI into a single package
          priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
          test: /[\\/]node_modules[\\/]_?vant(.*)/, // in order to adapt to cnpm
        },
        commons: {
          name: "chunk-commons",
          test: resolve("src/components"), // can customize your rules
          minChunks: 1, //  minimum common number
          priority: 5,
          reuseExistingChunk: true,
        },
      },
    });
```



#### yarn和npm命令对比

|                          npm | yarn                 |
| ---------------------------: | :------------------- |
|                  npm install | yarn                 |
|     npm install react --save | yarn add react       |
|   npm uninstall react --save | yarn remove react    |
| npm install react --save-dev | yarn add react --dev |
|            npm update --save | yarn upgrade         |
|                  npm run dev | yarn dev             |
|                npm run build | yarn build           |



#### 启动

```shell
yarn dev 			#本地开发启动 加载 .env.development配置
yarn dev:test 		#本地开发 调用 测试环境api 加载 .env.testing配置
yarn dev:preview 	#本地 预发环境调用
yarn prod 			#本地 线上环境 调试
```

#### 打包

```shell
yarn build:local	#打包本地环境 加载 .env.development配置
yarn build:test     #测试环境
yarn build:preview  #预发
yarn build			#线上
```

