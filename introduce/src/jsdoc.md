# Jsdoc规范

> 注释规范，尽量避免非比要的注释。
>
> JSDoc有2个作用，导出API文档和明确代码类型，辅助代码提示。



#### 示例

```javascript
/**
 * 绑定类型
 * @enum
 * @readonly
 */
export const BindType = {
  /**绑定 */
  BIND: 1,
  /**创建 */
  CREATE: 2,
};
```

```javascript
/**
 * 判断 数组target 是否完全 存在于 arr数组里
 * @param {Array} checkArr 需要判断的数组
 * @param {Array} target 目标数组
 * @returns {Array<Boolean, Boolean>} [all, part] all 全部包含 part 部分包含
 */
export function isContained(checkArr, target) {
  if (!_.isArray(checkArr) || !_.isArray(target))
    throw "checkArr or target not Array";
  let targetCache = {};
  let array = [];
  /** target to hashMap */
  target.forEach((item) => (targetCache[item] = true));

  checkArr.forEach((_id) => array.push(!!targetCache[_id]));

  let part, all;
  part = array.includes(true);
  all = !array.includes(false);

  return [all, all ? false : part];
}
```

```javascript
/**
 * 绑定用户
 * @param {number} childType 1 文亚  2导毕
 * @param {number} ucId
 * @param {string} userId 子系统学生ID
 * @param {string} bindType 1绑定  2创建   如果选择创建 则不需要选择childType
 * @function
 */
export const bindStudUser = (data) => {
  return request({
    url: `/uc/ucbind/v1/bindStudUser`,
    method: "post",
    data,
  });
};
```

