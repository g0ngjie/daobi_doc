# Vscode插件

> 个人开发习惯，推荐一些比较好用的插件

#### Git History

> GitLens 能够增强 Visual Studio Code 中内置的 Git 功能。它不仅能帮助你通过 Git blame 注解直观地看到代码作者，而且还可以无缝浏览和探索 Git 存储库，通过强大的比较命令获得有价值的见解等等。

#### GitLens

> git多人协作的时候需要查看日志，如果能在当前代码中查看到那是很方便的一件事，能省去很多时间去其他工具查看，提高工作效率。

#### Vetur

> 这个不用说了。vue官方插件，格式化工具有很多，但如果团队协作开发，尽量用同一种就可以了。

#### TODO Highlight

> TODO: 语法高亮，不过多解释。

#### Todo Tree

> 结合**TODO Highlight**，开发效率很高。

#### npm Intellisense

> 包引用支持友好