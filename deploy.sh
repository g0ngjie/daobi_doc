#!/bin/sh
# auth: Gj
# docker automated scripts
# 2020-07-28

TIME=$(date "+%m-%d %H:%M:%S")
DOCKER_IMAGE='daobidoc_image'
DOCKER_CONTAINER='daobidoc'

# help
show_help() {
    echo -e "\n     Commands:       Introduce:\n"
    echo -e "         start       [start]     docker-compose integrated deployment"
    echo -e "       restart       [restart]   docker-compose repackage compilation"
    echo -e "        docker       [start]     docker shell script deployment"
    echo -e "docker restart       [restart]   docker shell script redeployment"
    echo -e "          kill       [kill]      kill containers and mirrors"
    echo -e " help | -h | *       [help]      help documentation\n"
}

# docker-compose redeployment
docker_compose_redeployment() {
    # br=$(git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3)
    # git pull origin $br \
    # && docker-compose down \
    docker-compose down \
    && docker rmi -f $DOCKER_IMAGE \
    && docker-compose build \
    && docker-compose up -d
}

# docker start
docker_start() {
    docker build -t $DOCKER_IMAGE . \
    && docker run -d -p 12345:80 -p 12346:8080 -p 12347:8081 -p 12348:8082 -p 12349:8083 --name $DOCKER_CONTAINER $DOCKER_IMAGE
}

# docker redeployment
docker_redeployment() {
    docker_kill \
    && docker_start
}

# kill containers and mirrors
docker_kill() {
    docker stop $DOCKER_CONTAINER \
    && docker rm -f $DOCKER_CONTAINER \
    && docker rmi -f $DOCKER_IMAGE
}

case $1 in
    'start')
    docker-compose up -d
    echo -e "\n ---> $TIME [start] docker-compose script ran successfully\n"
    ;;
    'restart')
    docker_compose_redeployment
    echo -e "\n ---> $TIME [restart] docker-compose script ran successfully\n"
    ;;
    'docker')
    docker_start
    echo -e "\n ---> $TIME [start] docker script ran successfully\n"
    ;;
    'docker restart')
    docker_redeployment
    echo -e "\n ---> $TIME [restart] docker script ran successfully\n"
    ;;
    'kill')
    docker_kill
    echo -e "\n ---> $TIME [kill] containers and mirrors killed successfully\n"
    ;;
    '-h')
    show_help
    ;;
    *)
    show_help
esac
